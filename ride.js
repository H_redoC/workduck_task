import * as userDB from './user.js'

/** 
* In memory data structure for storing rides
* @type {Array} 
* */
let Rides = []

/**
* @throws throws error if ride already offered or user does not exist
* @param {Object} ride - details of ride offered
*/
function addRide(ride) {
  let username = ride.offeredBy
  let user = userDB.getUser(username)
  if(user){
    if(hasUnofferedVehicle(user, ride.vehicleNo)){
      Rides.push(ride)
      user.ridesOffered++;
      let vehicle = user.vehicles.find((v) => v.vehicleNo === ride.vehicleNo)
      vehicle.rideOffered = true
    }else {
      throw new Error("Ride already offered by this user for this vehicle")
    }
  }else {
    throw new Error("No user with this username")
  }
}

/**
* @param {string} vehicleNo - string representation of vehicleNo
* @returns {(Object|undefined)} returns ride if found or undefined if not found
*/
function getRide(vehicleNo) {
  return Rides.find((r) =>  r.vehicleNo === vehicleNo)
}

/**
* @param {Object} user - registered user
* @param {string} vehicleNo - string representation of vehicleNo
* @returns {Boolean} true if ride is not offered yet, else false
* @throws throws exception if vehicle not found
*/
function hasUnofferedVehicle(user, vehicleNo) {
  let vehicle = user.vehicles.find((v) => v.vehicleNo === vehicleNo)
  if(vehicle === undefined) {
    throw new Error("No such vehicle found")
  }
  return !vehicle.rideOffered
}

/**
* @param {Object} rideRequest - details of requested ride
* @returns {Object} returns an object containing best match ride
* @throws throws exception if
*   a) ride not found
*   b) preferred vehicle not found
*   c) not enough available seats
*/
function selectRide(rideRequest) {
  let bestMatch = Rides.filter((r) => {
    return (r.origin === rideRequest.origin && r.destination === rideRequest.destination)
  })
  if(bestMatch.length === 0) {
    throw new Error("No ride found")
  }
  if(rideRequest.strategy === "Most Vacant") {
    bestMatch.sort((a, b) => b.avlSeats - a.avlSeats)
  }else {
    // console.log(bestMatch, rideRequest.preferredVehicle)
    bestMatch = [bestMatch.find((v) => v.vehicle === rideRequest.preferredVehicle)]

    if(bestMatch === undefined) {
      throw new Error("No preferred Vehicle found")
    }
  }
  if(bestMatch[0].avlSeats >= rideRequest.seats){
    let user = userDB.getUser(rideRequest.passengerName)
    user.ridesTaken++
    bestMatch[0].avlSeats -= rideRequest.seats
    return bestMatch[0]
  }else {
    throw new Error("Not enough available seats")
  }
}

/**
* Ends specified ride
* @param {string} vehicleNo - string representation of vehicleNo
* @throws throws error if ride does not exist
*/
function endRide(vehicleNo) {
  let index = Rides.findIndex((r) => vehicleNo === r.vehicleNo)
  if(index === -1) throw new Error("Ride does not exist")
  Rides.splice(index, 1)
}

export {addRide, getRide, selectRide, endRide}
