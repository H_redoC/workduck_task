# Setup
* Clone the repo
* cd into the folder and run `yarn install`

# How to run
* `yarn test` will run all test cases and print the result
* `yarn doc *.js` will generate the documentation for the js files in `./out/` folder
* To view the documentation open `./out/global.html` in your browser.

# Note
* you can use **npm** if **yarn** is not installed on your system, just replace yarn with npm above.
