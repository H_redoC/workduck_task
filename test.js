import test from 'ava'

import * as userDB from './user.js'
import * as rideDB from './ride.js'
import * as tc from './testcases.js'

test.serial('Add Users', (t) => {
  tc.testUsers.forEach((tu) => {
    userDB.addUser(tu)
    let user = userDB.getUser(tu.name)
    t.is(user, tu)
  })
})

test.serial('Throw exception if user already exists', (t) => {
  try{
    userDB.addUser(tc.testUser)
    t.fail("No execption created by addUser function")
  }catch(e){
    t.is(e.message, "User already exists in db")
  }
})

test.serial('Add vehicle to the specified user', (t) => {
  tc.testVehicle.forEach((v) => {
    userDB.addVehicle(v.owner, v)
    let userVehicle = userDB.getUser(v.owner).vehicles
    t.is(v, userVehicle[userVehicle.length - 1])
  })
})

test.serial('Adding duplicate vehicle to same user should throw error', (t) => {
  let username = "Shashank"
  try{
    userDB.addVehicle(username, tc.testVehicle[0])
    t.fail("No exception occured")
  }catch(e){
    t.is("Vehicle already registered with this user", e.message)
  }
})

test.serial('offer rides to users', (t) => {
  tc.testRides.forEach((r) => {
    rideDB.addRide(r)
    let ride = rideDB.getRide(r.vehicleNo)
    t.is(r, ride)
  })
})

test.serial('adding ride for non registered user should throw exception', (t) => {
  try{
    rideDB.addRide(tc.testRideWithNonRegUser)
    t.fail("exception didn't occur")
  }catch(e){
    t.is("No user with this username", e.message)
  }
})

test.serial('Adding ride with already offered vehicle should throw exception', (t) => {
  try{
    rideDB.addRide(tc.testRideWithRegUser)
    t.fail("exception didn't occur")
  }catch(e){
    t.is("Ride already offered by this user for this vehicle", e.message)
  }
})

test.serial('selectRide with Most Vacant strategy', (t) => {
  let selectRideTestCase = {
    passengerName: "Nandini",
    origin: "Banglore",
    destination: "Mysore",
    seats: 1,
    strategy: "Most Vacant",
  }

  let ride = rideDB.selectRide(selectRideTestCase)
  t.is(JSON.stringify(ride), JSON.stringify({
    offeredBy: "Shipra",
    origin: "Banglore",
    destination: "Mysore",
    avlSeats: 1,
    vehicleNo: "KA-05-41491",
    vehicle: "Polo",
  }))
})

test.serial('selectRide with preferred vehicle', (t) => {
  let selectRideTestCase = {
    passengerName: "Rohan",
    origin: "Hyderabad",
    destination: "Banglore",
    seats: 1,
    strategy: "Preferred Vehicle",
    preferredVehicle: "Baleno",
  }
  let ride = rideDB.selectRide(selectRideTestCase)
  t.is(JSON.stringify(ride), JSON.stringify({
    offeredBy: "Shashank",
    origin: "Hyderabad",
    destination: "Banglore",
    avlSeats: 1,
    vehicleNo: "TS-05-62395",
    vehicle: "Baleno",
  }))
})

test('End an offered ride', (t) => {
  let vehicleNo = "KA-01-12345"
  rideDB.endRide(vehicleNo)
  let ride = rideDB.getRide(vehicleNo)
  if(ride !== undefined){
    t.fail("Ride not ended")
  }
  t.pass("Ride ended successfully")
})

test('Ending a non-existent ride should throw exception', (t) => {
  try{
    rideDB.endRide("TK-01-13455")
    t.fail("No exception occured")
  }catch(e){
    t.is("Ride does not exist", e.message)
  }
})

test('print stats', (t) => {
  tc.testSelectRides.forEach((sr) => {
    rideDB.selectRide(sr)
  })
  let userStats = userDB.stats()
  t.is(JSON.stringify(userStats), JSON.stringify([
    { name: 'Shashank', ridesTaken: 0, ridesOffered: 1 },
    { name: 'Rohan', ridesTaken: 1, ridesOffered: 1 },
    { name: 'Nandini', ridesTaken: 1, ridesOffered: 0 },
    { name: 'Shipra', ridesTaken: 0, ridesOffered: 2 },
    { name: 'Gaurav', ridesTaken: 1, ridesOffered: 0 },
    { name: 'Rahul', ridesTaken: 0, ridesOffered: 1 }
  ]))
})
