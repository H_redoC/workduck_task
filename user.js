// In Memory Data Structures

/**
* In memory data structure for storing information about users
* @type {Map}
*/
let Users = new Map()

/**
* Add user in In-Memory Data Structure
* @param {Object} User - Details of User
*/
function addUser(user) {
  if(Users.has(user.name)) {
    throw new Error("User already exists in db")
  }
  Users.set(user.name, user)
}

/**
* Search user by username
* @param {string} username - name of the user
* @returns {Object} user stored in Users Map
*/
function getUser(username) {
  let user = Users.get(username)
  return user
}

/**
* @param {string} username - name of the user
* @param {Object} vehicle - details of vehicle to be added
* @throws throws exception if User does not exist or Vehicle already registered
*/
function addVehicle(username, vehicle) {
  let user = getUser(username)
  if(user === undefined) {
    throw new Error("User does not exist: " + username)
  }
  // console.trace(user, "=======================")
  user.vehicles.forEach((v) => {
    if(v.vehicleNo === vehicle.vehicleNo) {
      throw new Error("Vehicle already registered with this user")
    }
  })
  user.vehicles.push(vehicle)
}

/**
* @returns {Object} An object containing stats of all users
*/
function stats() {
  let statsObj = []
  Users.forEach((u) => {
    statsObj.push({
      name: u.name,
      ridesTaken: u.ridesTaken,
      ridesOffered: u.ridesOffered,
    })
  })
  return statsObj
}

export {addUser, getUser, addVehicle, stats}
