let testUsers = [
  {
    name: "Shashank",
    gender: "M",
    age: 29,
    ridesTaken: 0,
    ridesOffered: 0,
    vehicles: [],
  },
  {
    name: "Rohan",
    gender: "M",
    age: 36,
    ridesTaken: 0,
    ridesOffered: 0,
    vehicles: [],
  },
  {
    name: "Nandini",
    gender: "F",
    age: 29,
    ridesTaken: 0,
    ridesOffered: 0,
    vehicles: [],
  },
  {
    name: "Shipra",
    gender: "F",
    age: 27,
    ridesTaken: 0,
    ridesOffered: 0,
    vehicles: [],
  },
  {
    name: "Gaurav",
    gender: "M",
    age: 29,
    ridesTaken: 0,
    ridesOffered: 0,
    vehicles: [],
  },
  {
    name: "Rahul",
    gender: "M",
    age: 35,
    ridesTaken: 0,
    ridesOffered: 0,
    vehicles: [],
  }
]
  
let testUser = {
  name: "Shashank",
  gender: "M",
  age: 29,
  ridesTaken: 0,
  ridesOffered: 0,
  vehicles: []
}

let testVehicle = [
  {owner: "Shashank", vehicleName: "Baleno", vehicleNo: "TS-05-62395", rideOffered: false},
  {owner: "Rohan", vehicleName: "Swift", vehicleNo: "KA-01-12345", rideOffered: false},
  {owner: "Shipra", vehicleName: "Polo", vehicleNo: "KA-05-41491", rideOffered: false},
  {owner: "Shipra", vehicleName: "Activa", vehicleNo: "KA-12-12332", rideOffered: false},
  {owner: "Rahul", vehicleName: "XUV", vehicleNo: "KA-05-1234", rideOffered: false},
]

let testRides = [
  {
    offeredBy: "Rohan",
    origin: "Hyderabad",
    destination: "Banglore",
    avlSeats: 1,
    vehicleNo: "KA-01-12345",
    vehicle: "Swift",
  },
  {
    offeredBy: "Shipra",
    origin: "Banglore",
    destination: "Mysore",
    avlSeats: 1,
    vehicleNo: "KA-12-12332",
    vehicle: "Activa",
  },
  {
    offeredBy: "Shipra",
    origin: "Banglore",
    destination: "Mysore",
    avlSeats: 2,
    vehicleNo: "KA-05-41491",
    vehicle: "Polo",
  },
  {
    offeredBy: "Shashank",
    origin: "Hyderabad",
    destination: "Banglore",
    avlSeats: 2,
    vehicleNo: "TS-05-62395",
    vehicle: "Baleno",
  },
  {
    offeredBy: "Rahul",
    origin: "Hyderabad",
    destination: "Banglore",
    avlSeats: 5,
    vehicleNo: "KA-05-1234",
    vehicle: "XUV",
  },
]

let testSelectRides = [
  {
    passengerName: "Gaurav",
    origin: "Banglore",
    destination: "Mysore",
    seats: 1,
    strategy: "Preferred Vehicle",
    preferredVehicle: "Activa"
  },
]

let testRideWithNonRegUser = {
  offeredBy: "Harsh",
  origin: "Mumbai",
  destination: "Banglore",
  avlSeats: 2,
  vehicleNo: "TS-05-62395",
  vehicle: "Baleno",
}

let testRideWithRegUser = {
  offeredBy: "Shashank",
  origin: "Mumbai",
  destination: "Banglore",
  avlSeats: 2,
  vehicleNo: "TS-05-62395",
  vehicle: "Baleno",
}

export {testUsers, testUser, testVehicle, testRides, testRideWithNonRegUser, testRideWithRegUser, testSelectRides}
